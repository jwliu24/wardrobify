# Generated by Django 4.0.3 on 2023-03-03 20:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_alter_shoe_options_rename_name_binvo_closet_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='binvo',
            name='bin_number',
            field=models.PositiveSmallIntegerField(),
        ),
        migrations.AlterField(
            model_name='binvo',
            name='bin_size',
            field=models.PositiveSmallIntegerField(),
        ),
    ]
