import { BrowserRouter, Routes, Route, useLinkClickHandler } from 'react-router-dom';
import HatList from './HatList';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';

function App(props) {
  if (props.shoes === undefined && props.hats === undefined) {
    return null;
  }


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="new" element={<ShoeForm />} />
          </Route>
          <Route path="shoes">
            <Route path="" element={<ShoeList shoes={props.shoes} />} />
          </Route>
          <Route path="hats">
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="hats">
            <Route path="" element={<HatList hats={props.hats} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
