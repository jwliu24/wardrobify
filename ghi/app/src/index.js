import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadData () {
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatResponse = await fetch('http://localhost:8090/api/hats/');
  if (shoeResponse.ok && hatResponse.ok) {
    const shoeData = await shoeResponse.json();
    const hatData = await hatResponse.json();
    root.render(
      <React.StrictMode>
        <App shoes={shoeData.shoes} hats={hatData.hats} />
      </React.StrictMode>
    );
  } else {
    console.error(shoeResponse);
    console.error(hatResponse);
  }
}
loadData();
